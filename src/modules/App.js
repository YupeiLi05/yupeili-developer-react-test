import React, {useEffect} from 'react'
import './i18n/i18n';
import {Provider} from 'react-redux'
import {Route, Switch, Router} from 'react-router-dom'
import Header from './layout/Header'
import Footer from './layout/Footer'
import SidebarScreen from './layout/SidebarScreen'
import Registration from './auth/Registration'
import Login from './auth/Login'
import ForgotPassword from './auth/ForgotPassword'
import {Helmet} from 'react-helmet'
import Privacy from "./apps/terms/Privacy"
import Terms from "./apps/terms/Terms"
import GlobalStyle from './styles/GlobalStyle'
import AppThemeProvider from './styles/AppThemeProvider'
// eslint-disable-next-line no-unused-vars
import icons from './common/icons'
import ConfigContainer from './config/ConfigContainer'
import config from '../config'
import NavHider from './common/components/NavHider'
import NotFoundScreen from './common/components/NotFoundScreen'
import '../../node_modules/react-smartbanner/dist/main.css'
const AppRoot = ({history, store})=>{
  useEffect(() =>{
    function lsTest() {
      var test = 'test'
      try {
        localStorage.setItem(test, test)
        localStorage.removeItem(test)
        return true
      } catch (e) {
        return false
      }
    }
    if (lsTest() === true) {}
    else {
      alert("Your browser does not have cookies and local storage enabled. Please enable them to log in to the application.")
    }
  }, [])
  const hideNavRoutes = ['/apps/:appId/activities/:activityId']
  return (<Provider store={store}>
    <ConfigContainer.Provider initialState={{config}}>
      <Router history={history}>
        <AppThemeProvider>
          <GlobalStyle/>
            <Helmet>
                <title>{config.appName}</title>
            </Helmet>
            <NavHider routes={hideNavRoutes}>
              <Header/>
            </NavHider>
            <Switch>
              <Route exact={true} path="/(|login)/" component={Login}/>
              <Route exact={true} path="/register" component={Registration}/>
              <Route exact={true} path="/forgot" component={ForgotPassword}/>
              <Route exact={true} path="/privacy" render={() => <SidebarScreen><Privacy/></SidebarScreen>}/>
              <Route exact={true} path="/terms" render={() => <SidebarScreen><Terms/></SidebarScreen>}/>
              <Route exact={true} component={NotFoundScreen}/>
            </Switch>
            <NavHider routes={hideNavRoutes}>
              <Footer/>
            </NavHider>
          </AppThemeProvider>
      </Router>
    </ConfigContainer.Provider>
  </Provider>)
}
export default AppRoot
