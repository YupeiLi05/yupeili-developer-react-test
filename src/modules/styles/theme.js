const white = '#FFFFFF'
const black = '#222534'
const darkGrey = '#3A4048'
const grey = '#616771'
const lightGrey = '#969EAA'
const lighterGrey = '#D6D6D6'
const offWhite = '#F1F1F1'
const blue = '#0671B2'
const mediumGrey = '#E4E4E4'
const red = '#DB1919'
const orange = '#D14900'
const green = '#008542'
const purple = '#7A35C1'
const yellow = '#FFD515'
const aqua = '#17837E'
const darkerGrey = '#363636'
const blueHover = '#4BAEEA'
const bluePressed = '#06355F'
const blueHoverPale = '#D7E7F7'
const primary = blue
const secondary = aqua
const dark = darkerGrey
const light = white
const primaryPale = blueHoverPale

const theme = {
  font: "'Montserrat', sans-serif",
  fontWeight: 400,
  fontWeightBold: 700,
  colors:{
    primary:primary,
    secondary:secondary,
    dark:dark,
    light:light,
    darkGrey:darkGrey,
    primaryPale:primaryPale,
    grey:grey,
    pale:lighterGrey,
    black:black,
    white:white,
    lightGrey:lightGrey,
    offWhite:offWhite,
    red:red,
    orange:orange,
    yellow:yellow,
    purple:purple,
    green:green,
    mediumGrey:mediumGrey,
  },
  header:{
    height: 58,
    background: black,
    logo:{
      width: 34,
      height: 34,
      url:'/images/logo.png',
    }
  },
  menu:{
    width:221,
  },
  buttons:{
    borderRadius:0,
    default:{
      background:white,
      border:primary,
      color:primary,
      hover:{
        border:blueHover,
      },
      press:{
        border:bluePressed,
      },
    },
    primary:{
      background:primary,
      color:white,
      hover:{
        background:blueHover,
      },
      press:{
        background:bluePressed,
      },
    },
  },
}
export default theme
