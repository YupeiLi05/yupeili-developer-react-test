import { createGlobalStyle } from 'styled-components'
import Color from 'color'
const getHoverColor = (color) => Color(color).lighten(0.1).saturate(0.5).hex()
const getPressColor = (color) => Color(color).lighten(0.5).saturate(0.5).hex()
const GlobalStyle = createGlobalStyle`
  html{
    height: 100%;
    width:100%;
  }
  body{
    font-family: ${props => props.theme.font};
    font-weight: ${props => props.theme.fontWeight};
    height: 100%;
    width:100%;
    display: flex;
    min-height: 100vh;
    flex-direction: column;
    font-size:14px;
    background-color:${props => props.theme.colors.offWhite};
    margin: 0;
    // padding-top: 64px;
    #root{
      height:100%;
      width:100%;
    }
    nav{
      .container{
        width: 100%;
        padding: 0 30px;
      }
    }
    h1, h2, h3, h4, h5, h6{
      margin-top:0
      font-weight: ${props => props.theme.fontWeightBold};
    }
    h2{
      font-size:20px;
      @media (min-width: 768px) {
        font-size:28px;
      }
    }
    h3{
      font-size:14px;
      @media (min-width: 768px) {
        font-size:16px;
      }
    }
    h4{
      font-size:12px;
      @media (min-width: 768px) {
        font-size:14px;
      }
    }
    label{
      font-weight:${props => props.theme.fontWeightBold};
      font-size:14px;
    }
    //bootstrap theme
    hr{
      border-top-color:${props => props.theme.colors.primary};
    }
    a{
      background:none;
      border:none;
      color:${props => props.theme.colors.primary};
      &:hover, &:focus{
        color:${props => props.theme.colors.dark};
        text-decoration: underline;
      }
    }
    //input
    input{
      font-size:14px;
      border-radius:0;
      padding: 8px 12px;
      height:2.5em;
      max-width:445px;
    }
    input[type="radio"]{
      font-size:14px;
      padding: 8px 12px;
      height:auto;
    }
    input[type="checkbox"]{
      font-size:14px;
      padding: 8px 12px;
      height:auto;
    }
    textarea{
      font-size:14px;
      border-radius:0;
      padding: 8px 12px;
    }
    //select
    select{
      font-size:14px;
      border-radius:0;
      padding: 8px 12px;
      height:2.5em;
      color:${props => props.theme.colors.black};
      border-radius:0;
      appearance: none;
      display:block;
      width:100%;
      background-color:white;
      position:relative;
      option:{
        &:hover{
          background-color:${props => props.theme.colors.primaryPale};
        }
      }
      &:after{
        position: absolute;
        content: "";
        right: 0;
        top: 5px;
        width: 10px;
        height: 10px;
        border-top: 10px solid red;
        border-left: 10px solid transparent;
        border-right: 10px solid transparent;
        border-bottom: 10px solid transparent;
      }
    }
    .table{
      font-size:14px;
      tr{
        td{
          padding:13px;
        }
      }
      tbody{

      }
      &.table-hover{
        tbody{
          tr{
            cursor:pointer;
            &:hover{
              background-color:${props => props.theme.colors.primaryPale};
            }
          }
        }
      }
    }
    // apply css rules directly for draggable table rows outside of table
    tr {
      background-color:${props => props.theme.colors.white};
      td {
        padding: 8px;
        line-height: 1.428571429;
        vertical-align: top;
        border-top: 1px solid #ddd;
      }
    }
    .form-control{
      border-radius:0;
      border: 1px solid ${props => props.theme.colors.grey};
      color:${props => props.theme.colors.black};
      &:focus{
        border-color:${props => props.theme.colors.primary};
        box-shadow:none;
      }
      &.error{
        border-color:${props => props.theme.colors.orange};
      }
    }
    //buttons
    .btn{
      background: ${props => props.theme.colors.white};
      border: ${props => '2px solid '+props.theme.colors.primary};
      color: ${props => props.theme.colors.primary};
      outline: none;
      font-size: 16px;
      font-weight: ${props => props.theme.fontWeightBold};
      padding: 10px 40px;
      border-radius: ${props => props.theme.buttons.borderRadius};
      // text-transform: uppercase;
      min-width: 152px;
      position: relative;
      &:hover{
        background: ${props =>props.theme.colors.white};
        border-color: ${props => getHoverColor(props.theme.colors.primary)};
        color: ${props => getHoverColor(props.theme.colors.primary)};
      }
      &:focus{
        background: ${props => props.theme.colors.white};
        border-color: ${props => '2px solid '+getHoverColor(props.theme.colors.primary)};
        color: ${props => getHoverColor(props.theme.colors.primary)};
      }
      &:active{
        background: ${props => props.theme.colors.white};
        border-color: ${props => '2px solid '+getPressColor(props.theme.colors.primary)};
        color: ${props => getPressColor(props.theme.colors.primary)};
        box-shadow: none;
      }
      &[disabled], &:disabled{
        opacity: .65;
        cursor: not-allowed;
        background: ${props => props.theme.colors.white};
        border: ${props => '2px solid '+props.theme.colors.primary};
      }
      .icon-before{
        position: absolute;
        top: .8em;
        left: 1em;
      }
      .icon-after{
        position: absolute;
        top: .8em;
        right: 1em;
      }
      .spinner{
        position: absolute;
         top: 0.6em;
         right: 0.6em;
      }
      &.btn-primary{
        background: ${props => props.theme.colors.primary};
        border: none;
        color: ${props => props.theme.colors.white};
        padding:${props => '12px 42px'};
        min-width: ${props => '156px'};
        &:hover{
          background: ${props =>  getHoverColor(props.theme.colors.primary)};
        }
        &:focus{
          background: ${props => getHoverColor(props.theme.colors.primary)};
        }
        &:active{
          background: ${props => getPressColor(props.theme.colors.primary)};
        }
        &[disabled], &:disabled{
          background: ${props => props.theme.colors.primary};
        }
      }
      &.btn-danger{
        background: ${props => props.theme.colors.red};
        border: none;
        color: ${props => props.theme.colors.white};
        padding:${props => '12px 42px'};
        min-width: ${props => '156px'};
        &:hover{
          background: ${props =>  getHoverColor(props.theme.colors.red)};
        }
        &:focus{
          background: ${props => getHoverColor(props.theme.colors.red)};
        }
        &:active{
          background: ${props => getPressColor(props.theme.colors.red)};
        }
        &[disabled], &:disabled{
          background: ${props => props.theme.colors.red};
        }
      }
      &.btn-m {
        padding: 7px 20px;
        font-size: 14px;
      }
      &.btn-sm{
        padding: 5px 10px;
        font-size: 12px;
      }
    }
    .dropdown-toggle{
      min-width:0;
      padding: 6px 12px;
      border:none;
      background:transparent;
      &:focus{
        background:transparent;
      }
      &:active{
        background:transparent;
      }
      &[disabled], &:disabled{
        background:transparent;
      }
    }
    //panel
    .panel{
      background-color:${props => props.theme.colors.white};
      border:0;
      box-shadow:none;
      border-radius:0;
      padding:19px 14px 14px;
      position:relative;
      &.content-panel{
        padding:26px 28px 30px;
      }
      &.line-above{
        &:before{
          background-color:${props => props.theme.colors.primary};
          content: " ";
          width: 100%;
          height: 5px;
          display: block;
          position: absolute;
          left: 0;
          top: 0;
        }
      }
      .panel-heading{
        .panel-title{
          width: 100%;
          display: flex;
          align-items: center;
          justify-content: space-between;
          #chars{
            width: 100%;
            margin: 0 10px;
          }
        }
      }
    }
    //modal
    .modal{
      overflow-y:scroll;
      .modal-dialog{
        margin:0;
        @media (min-width: 768px) {
          margin:30px auto;
        }
        .modal-content{
          border-radius:0;
          box-shadow:none;
          background-color:${props => props.theme.colors.white};
          @media (min-width: 768px) {
            border-radius:15px;
          }
          .modal-header{
            background-color:${props => props.theme.colors.offWhite};
            text-align: center;
            border: none;
            font-weight:${props => props.theme.fontWeightBold};
            @media (min-width: 768px) {
              border-radius: 0;
            }
            .close{
              background-color:${props => props.theme.colors.white};
              width: 30px;
              height: 30px;
              border-radius: 25px;
              border:1px solid ${props => props.theme.colors.pale};
              color: ${props => props.theme.colors.primary};
              opacity: 1;
              font-size: 24px;
              &:hover, &:active{
                background-color:${props => props.theme.colors.pale};
                text-shadow:none;
              }
            }
          }
          .modal-body{
            background-color:${props => props.theme.colors.white};
            height: 60vh;
            overflow: scroll;
          }
          .modal-footer{
            text-align: center;
            background-color:white;
            @media (min-width: 768px) {
              border-radius:0 0 15px 15px;
            }
          }
        }
      }
    }
    //list-group
    .list-group{
      // border: 1px solid ${props => props.theme.colors.pale};
      border-radius:0;
      .list-group-item{
        border-radius:0;
          // border: 1px solid ${props => props.theme.colors.pale};
      }
    }
    //tabs
    .tabs{
      border:none;
      .nav-tab{
        display:inline-block;
        padding:11px 20px;
        background-color:${props => props.theme.colors.pale};
        color: ${props => props.theme.colors.black};
        border-radius:0;
        border:none;
        border-top:5px solid ${props => props.theme.colors.pale};
        margin-right:10px;
        &:hover{
          background-color:${props => props.theme.colors.pale};
          border-color: ${props => props.theme.colors.pale};
          color: ${props => props.theme.colors.primary};
          text-decoration:none;
        }
        &:focus{
          color: ${props => props.theme.colors.primary};
          text-decoration:none;
        }
        &:active{
          color: ${props => props.theme.colors.primary};
          text-decoration:none;
        }
        &[disabled], &:disabled{
          color: ${props => props.theme.colors.black};
        }
        &.active{
          border-color: ${props => props.theme.colors.primary};
          background-color:${props => props.theme.colors.white};
        }
      }
    }
    .tab-content{
      background-color:${props => props.theme.colors.white};
      padding:20px;
      border: 1px solid #ddd;
      border-top: none;
      .tabs{
        border-bottom: 1px solid ${props => props.theme.colors.lightGrey};
        .nav-tab{
          border-top: 1px solid ${props => props.theme.colors.white};
          border-left: 1px solid ${props => props.theme.colors.white};
          border-right: 1px solid ${props => props.theme.colors.white};
          border-bottom: 1px solid ${props => props.theme.colors.lightGrey};
          margin-bottom:-1px;
          &.active{
            border-top: 1px solid ${props => props.theme.colors.lightGrey};
            border-left: 1px solid ${props => props.theme.colors.lightGrey};
            border-right: 1px solid ${props => props.theme.colors.lightGrey};
            border-bottom: 1px solid ${props => props.theme.colors.white};
          }
        }
      }
    }
    //progress bar
    .progress{
      height:8px;
      .progress-bar{
        border-radius:4px;
        background-color:${props => props.theme.colors.secondary};
      }
    }
    //pagination
    .pagination{
      border-radius:0;
      li, li:first-child, li:last-child{
        a{
          border-color:${props => props.theme.colors.pale}
          border-radius:0;
          font-size:14px;
        }
        &.active{
          a{
            border-color:${props => props.theme.colors.primary};
            background-color:${props => props.theme.colors.primary};
            &:hover, &:focus{
              border-color:${props => getHoverColor(props.theme.colors.primary)};
              background-color:${props => getHoverColor(props.theme.colors.primary)};
            }
          }
        }
      }
    }
    //form alert
    .alert{
      text-align:left;
      color:${props => props.theme.colors.orange};
      background: transparent;
      border: none;
      margin: 0;
      padding: 10px 0;
      span{
        display: block;
      }
    }
    //datepicker
    .datepicker{
      border: 1px solid ${props => props.theme.colors.lightGrey};
      color: ${props => props.theme.colors.black};
      font-size: 14px;
      padding: 7px 19px;
      font-weight: ${props => props.theme.fontWeight};
      text-transform: none;
    }
    //columns
    .col-lg-1, .col-lg-10, .col-lg-11, .col-lg-12, .col-lg-2, .col-lg-3, .col-lg-4, .col-lg-5, .col-lg-6, .col-lg-7, .col-lg-8, .col-lg-9, .col-md-1, .col-md-10, .col-md-11, .col-md-12, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-sm-1, .col-sm-10, .col-sm-11, .col-sm-12, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-xs-1, .col-xs-10, .col-xs-11, .col-xs-12, .col-xs-2, .col-xs-3, .col-xs-4, .col-xs-5, .col-xs-6, .col-xs-7, .col-xs-8, .col-xs-9 {
      padding-left:5px;
      padding-right:5px;
    }
    //Breadcrumb
    .breadcrumb{
      font-size:16px;
      background:transparent;
      padding-left: 0;
      color: ${props => props.theme.colors.black};
      li{
        a{
          color: ${props => props.theme.colors.black};
        }
      }
      li+li{
        &:before{
          content:">"
        }
      }
    }
    //tooltip
    .tooltip{
      &.in{
        opacity:1;
      }
      .tooltip-inner{
        background-color: ${props => props.theme.colors.secondary};
        padding:9px 17px;
        font-size:13px;
      }
      &.top{
        .tooltip-arrow{
          border-top-color: ${props => props.theme.colors.secondary};
        }
      }
      &.bottom{
        .tooltip-arrow{
          border-bottom-color: ${props => props.theme.colors.secondary};
        }
      }
      &.left{
        .tooltip-arrow{
          border-left-color: ${props => props.theme.colors.secondary};
        }
      }
      &.right{
        .tooltip-arrow{
          border-right-color: ${props => props.theme.colors.secondary};
        }
      }
    }
  }
}
`;
export default GlobalStyle
