import React from 'react';
import {Control} from 'react-redux-form'
import {authRequirements} from '../common/constants'
import styled from 'styled-components'
import PasswordMask from 'react-password-mask';
import FontAwesomeIcon from '@fortawesome/react-fontawesome'
import {
  required,
  maxLength,
  minLength,
  containsLetter,
  containsNumber,
} from '../common/validators'
class Password extends React.Component {
  render() {
    const validators = {
      required: required,
      maxLength: maxLength(authRequirements.PASSWORD_MAX),
      minLength: minLength(authRequirements.PASSWORD_MIN),
      containsNumber: containsNumber,
      containsLetter: containsLetter
    }
    const {errors} = this.props
    const showButtonContent = <FontAwesomeIcon icon={['fal','eye']}/>
    const hideButtonContent = <FontAwesomeIcon icon={['fal','eye-slash']}/>
    return (<div className={this.props.className}>
      <div className="input-group">
        <Control model={this.props.model} validators={validators} className="form-control" inputClassName="password-input" placeholder="Password" required={true} maxLength={authRequirements.PASSWORD_MAX} component={PasswordMask} useVendorStyles={false} buttonClassName="show-hide-button" showButtonContent={showButtonContent} hideButtonContent={hideButtonContent} defaultValue='' errors={errors} />
      </div>
    </div>)
  }
}
const StyledComponent = styled(Password)`
  .input-group{
    width:100%;
    .form-control{
      width:100%;
      position:relative;
      padding:0;
      .password-input{
        width:100%;
        height: 100%;
        border: none;
        background: transparent;
      }
      .show-hide-button{
        position: absolute;
        right: 10px;
        top: 7px;
      }
    }
  }
`
export default StyledComponent
