import React, {useState} from 'react';
import {Link} from 'react-router-dom'
import {authRequirements} from '../common/constants'
import Title from '../common/components/Title'
import Input from '../common/hookforms/Input'
import Submit from '../common/hookforms/Submit'
import CreateAccountButton from '../common/components/CreateAccountButton'
import CenteredScreen from '../layout/CenteredScreen'
import {Form} from 'react-bootstrap'
import useForm from 'react-hook-form'
import styled from 'styled-components'
import AuthBanner from './AuthBanner'
const Wrapper = styled(CenteredScreen)`
  form{
    width:300px;
    text-align:left;
    margin:30px auto 15px;
    .submit{
      margin-top:42px;
      text-align:center;
    }
  }
`
const ForgotPassword = ({location, history}) => {
  const [sent, setSent] = useState(false)
  const form = useForm({})
  const onSubmit = (val) =>{
    setSent(true)
  }
  return (<Wrapper className="screen-login">
    <Title title="Log in"/>
    <div className="container-fluid">
      <AuthBanner/>
      <h2>Password reset</h2>
      {sent?
        (<div>
          <p>The password reset email has been sent to your email with further instructions.</p>
          <p>Please check your inbox!</p>
        </div>)
        :
      (<div>
        <p>Enter your Cogniss <strong>username</strong>, or the <strong>email address</strong> that you used to register.</p>
        <p>We'll send you an email with your username and a link to reset your password.</p>
        <Form horizontal onSubmit={form.handleSubmit(onSubmit)}>
          <Input name="username" form={form} label="Username or email" minLength={authRequirements.USERNAME_MIN} required/>
          <Submit className="submit" label="Reset password" form={form} primary/>
        </Form>
        <p>
          Don't have an account yet? &nbsp;
          <CreateAccountButton/>
        </p>
      </div>)
      }
      <p><Link to="/" className="btn-login">Return to the Login page</Link></p>
    </div>
  </Wrapper>)
}
export default ForgotPassword
