import React from 'react';
import {Link} from 'react-router-dom'
import {authRequirements} from '../common/constants'
import Title from '../common/components/Title'
import Input from '../common/hookforms/Input'
import Submit from '../common/hookforms/Submit'
import Password from '../common/hookforms/Password'
import CenteredScreen from '../layout/CenteredScreen'
import {Form} from 'react-bootstrap'
import useForm from 'react-hook-form'
import styled from 'styled-components'
import AuthBanner from './AuthBanner'
import CreateAccountButton from '../common/components/CreateAccountButton'

const Wrapper = styled(CenteredScreen)`
  form{
    width:300px;
    text-align:left;
    margin:0 auto 15px;
    .submit{
      margin-top:42px;
      text-align:center;
    }
  }
`
const Login = ({location, history}) => {
  const form = useForm({})
  const onSubmit = (val) =>{}
  
  return (<Wrapper className="screen-login">
    <Title title="Log in"/>
    <div className="container-fluid">
      <AuthBanner/>
      <Form horizontal onSubmit={form.handleSubmit(onSubmit)}>
        <Input name="username" form={form} label="Username" minLength={authRequirements.USERNAME_MIN} required/>
        <Password name="password" form={form} label="Password" required/>
        <Submit className="submit" label="Sign in" form={form} primary/>
      </Form>
      <p>
        <Link to="forgot" className="btn-forgot">Forgot your username or password?</Link>
      </p>
      <p>
        Don't have an account yet? &nbsp;
        <CreateAccountButton />
      </p>
    </div>
  </Wrapper>)
}
export default Login
