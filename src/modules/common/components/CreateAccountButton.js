import React, {useState} from 'react'
import {Link, withRouter} from 'react-router-dom'
import Modal from 'react-bootstrap/lib/Modal'
import Button from 'react-bootstrap/lib/Button'
import Panel from 'react-bootstrap/lib/Panel'
import Glyphicon from 'react-bootstrap/lib/Glyphicon'
import Tabs from 'react-bootstrap/lib/Tabs'
import Tab from 'react-bootstrap/lib/Tab'
import Checkbox from 'react-bootstrap/lib/Checkbox'

const Popup = withRouter(({show, handleClose, history}) => {
	//for modal open
	const [open, setOpen] = useState(false);
	//for check click
	const [check, setCheck] = useState(false);
	//when user click accept button
	const handleAccept = () => {
		handleClose();
		history.push("register");
	}
	
	return (
		<Modal show={show} onHide={handleClose}>
			<Modal.Header>
				Terms & Privacy Policy
			</Modal.Header>
			<Modal.Body>
				<Panel id="panel" expanded={open} onToggle={()=>{}}>
					<Panel.Heading onClick={()=>setOpen(!open)}>
						<Panel.Title>
							<Glyphicon glyph="info-sign" />
							<span id="chars">Statement of intent</span>
							<Glyphicon glyph="chevron-down" />
						</Panel.Title>
					</Panel.Heading>
					<Panel.Collapse>
						<Panel.Body>
							Anim pariatur cliche reprehenderit, enim eiusmod high life
							accusamus terry richardson ad squid. Nihil anim keffiyeh
							helvetica, craft beer labore wes anderson cred nesciunt sapiente
							ea proident.
						</Panel.Body>
					</Panel.Collapse>
				</Panel>

				<Tabs defaultActiveKey={0} id="tabs">
					<Tab eventKey={0} title="Terms of Use">
						<p>What is Lorem Ipsum?</p>
						<p>
							Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
						</p>
						<p>Why do we use it?</p>
						<p>
						It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).
						</p>
						<p>Where does it come from?</p>
						<p>
						Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.
						</p>
					</Tab>
					<Tab eventKey={1} title="Privacy Policy">
						<p>
						Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi in justo augue. In interdum pulvinar massa, eget tempus quam laoreet vitae. Nullam bibendum ligula a nibh consequat, id tincidunt massa scelerisque. Nam porttitor efficitur ex, ut volutpat tellus vehicula at. In placerat dui vel libero sodales, id sollicitudin mi rhoncus. Curabitur nibh nisi, malesuada eget hendrerit at, iaculis non nisl. Aliquam vulputate lacinia eros. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Interdum et malesuada fames ac ante ipsum primis in faucibus. Integer enim quam, egestas vel tempor eget, mollis in lorem. Vestibulum consequat, ipsum sed ultricies rhoncus, magna sem egestas urna, sed hendrerit lorem elit at enim. Quisque nec eros nisl. Fusce ex magna, hendrerit at maximus tempor, dignissim in lectus. Vivamus ut sollicitudin nisl, a viverra nisi.
						</p>
						<p>
						Donec vitae elit eget urna congue molestie a sed sapien. Nunc eu imperdiet quam, in porta elit. In sed lacus aliquam, tincidunt urna et, fringilla ligula. Praesent id justo scelerisque, interdum urna quis, viverra neque. Mauris dictum congue nisl, nec sodales lacus tempus eu. Suspendisse ex elit, sollicitudin vestibulum purus vitae, posuere porta est. Vestibulum iaculis ligula non condimentum convallis. Sed in lectus quis risus facilisis suscipit vel a dolor. Donec tempor lorem convallis nisi posuere cursus.
						</p>
						<p>
						Ut et nunc a risus congue finibus sit amet nec neque. Nunc sodales, leo vulputate ultrices varius, dolor nibh suscipit nunc, in vestibulum sem metus aliquam massa. Mauris mollis est dui, vel viverra lectus tristique at. Donec sollicitudin aliquam nisi et faucibus. Nullam nunc sem, laoreet in risus a, consectetur faucibus mauris. Etiam id ullamcorper augue, non convallis risus. Nunc odio tortor, efficitur id justo ut, tempus tincidunt nisi. Pellentesque at ante eget est imperdiet varius.
						</p>
						<p>
						Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur nulla risus, dictum at nisi ut, mollis ultricies enim. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Integer et dapibus est. Proin fermentum maximus varius. Mauris purus tellus, lobortis in tortor vel, malesuada lobortis libero. Donec quis porta ligula.
						</p>
						<p>
						Suspendisse sit amet lorem vel arcu rutrum mollis. Morbi porttitor ultrices est, vitae sodales ex venenatis ut. Praesent dolor leo, varius sed dui vel, maximus euismod magna. Proin eu iaculis ante. Nam consequat leo sit amet odio aliquet, sit amet convallis metus pharetra. Sed imperdiet, ipsum vel elementum aliquet, ligula turpis condimentum velit, vel viverra arcu tellus sed odio. Quisque placerat ante sed ex facilisis cursus. Donec bibendum tempor tellus quis hendrerit.
						</p>
					</Tab>
				</Tabs>

			</Modal.Body>
			<Modal.Footer>
				<Checkbox checked={check} onChange={()=>setCheck(!check)}>
					I have read and agree to the Terms and Privacy Policy
				</Checkbox>
				<Button variant="secondary" onClick={handleClose}>
					Close
				</Button>
				<Button variant="primary" bsStyle="primary" onClick={handleAccept} disabled={!check}>
					Accept
				</Button>
			</Modal.Footer>
		</Modal>
	)
})

const CreateAccountButton = () => {
	const [show, setShow] = useState(false);
  const onHandleClose = ()=>setShow(false);
  const onHandleOpen = (e)=> {
    e.preventDefault(); setShow(true)
  };

	return (
		<>
			<Popup show={show}  handleClose={onHandleClose} />
			<Link to="register" className="btn-registration" onClick={onHandleOpen}>Create an account?</Link>
		</>
	)
}

export default CreateAccountButton


