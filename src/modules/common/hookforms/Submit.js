import React from 'react';
import SubmitButton from './SubmitButton'
export default ({label, pending, className, primary})=>{
  return(<div className={className}>
    <SubmitButton label={label} pending={pending} primary={primary}/>
  </div>)
}
